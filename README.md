## Server application for Universal Dictionary

To compile application we need enter next command:
```sh
$ gradlew clean bootRepackage {APP_OPTS}
```
After that WAR file will be created in the */core/build/libs* folder.

Next, we can run the program with the following command:
```sh
$ java -jar {APP_OPTS} name.war
```
or deploying a WAR into a Tomcat/Jetty(etc.) container.

Also we can run an application:
```sh
$ gradlew clean bootRun {APP_OPTS}
```
## Profile
The application can be run in two modes: **dev** (contains swagger) and **prod**.
Swagger documentation available by the next path: **/documentation/swagger-ui.html**

**Important**: We should always set an active profile.

## Base {APP_OPTS}
```sh
-Dspring.profiles.active=   # dev or prod 
-Dport=                     # port of application
-Dprop.path=                # path to properties to setup application (redefine)
                            # For example: -Dprop.path=/job/config
```
## Properties
The default path to redefine properties is */etc/conf* folder. We can setup it through **{APP_OPTS}**.

The properties file must have a name like **override-{NAME OF THE PROFILE}.properties** (active profile).
For example: **override-prod.properties**.