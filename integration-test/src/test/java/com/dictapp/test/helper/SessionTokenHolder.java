package com.dictapp.test.helper;

import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Log4j
public class SessionTokenHolder {

    private final ThreadLocal<String> tokenStorage = new ThreadLocal<>();

    public void setCurrentToken(String token) {
        tokenStorage.set(token);
    }

    public String getCurrentToken() {
        return tokenStorage.get();
    }

    @PostConstruct
    public void init() {
        log.info("Session holder initialized!");
    }
}
