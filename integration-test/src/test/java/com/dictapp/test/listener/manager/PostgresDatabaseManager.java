package com.dictapp.test.listener.manager;

import com.google.common.base.Joiner;
import lombok.extern.log4j.Log4j;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Log4j
public class PostgresDatabaseManager extends AbstractDatabaseManager {

    public PostgresDatabaseManager(JdbcTemplate jdbcTemplate, String schema) {
        super(jdbcTemplate, schema);
    }

    @Override
    public void truncateAllTables() {
        List<String> tableNames = jdbcTemplate.query("SELECT * FROM pg_tables WHERE schemaname = ?",
                new Object[] {schema}, (rs, rowNum) -> rs.getString("tableName"));
        truncateTablesByName(tableNames);
    }

    public void truncateTablesByName(Collection<String> tableNames) {
        List<String> tableToTruncate = tableNames.stream()
                .filter(tableName ->  !tableName.equalsIgnoreCase(CHANGELOG_TABLE_NAME))
                .collect(Collectors.toList());

        log.info("Truncate Postgres tables: " + tableToTruncate.size() + " size");
        jdbcTemplate.execute("TRUNCATE TABLE " + Joiner.on(", ").join(tableToTruncate) + " CASCADE");
    }
}
