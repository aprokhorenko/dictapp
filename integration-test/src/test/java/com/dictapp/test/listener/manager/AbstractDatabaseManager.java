package com.dictapp.test.listener.manager;

import com.google.common.base.Joiner;
import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.FileSystemResourceAccessor;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Collection;

@AllArgsConstructor
public abstract class AbstractDatabaseManager implements DatabaseManager {

    static final String CHANGELOG_TABLE_NAME = "databasechangelog";

    protected final JdbcTemplate jdbcTemplate;
    protected final String schema;

    @Override
    public void runScripts(Collection<String> changeSetLocations, String dirName) throws SQLException, FileNotFoundException, LiquibaseException {
        DatabaseConnection database = new JdbcConnection(jdbcTemplate.getDataSource().getConnection());
        for (String location : changeSetLocations) {
            File file = ResourceUtils.getFile(this.getClass().getResource(dirName));
            Liquibase liquibase = new Liquibase(location, new FileSystemResourceAccessor(file.getAbsolutePath()), database);
            liquibase.update(new Contexts());
        }
    }

    public abstract void truncateTablesByName(Collection<String> tableNames);
}
