package com.dictapp.test.listener;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum DBMSType {
    POSTGRES("org.postgresql.Driver"),
    H2("org.h2.Driver");

    private final List<String> driverClasses;

    DBMSType(String... driverClasses) {
        this.driverClasses = Collections.unmodifiableList(Stream.of(driverClasses).collect(Collectors.toList()));
    }

    public static Optional<DBMSType> findBy(String driverClass) {
        return Stream.of(values())
                .filter(dbmsType -> dbmsType.driverClasses.contains(driverClass))
                .findFirst();
    }
}
