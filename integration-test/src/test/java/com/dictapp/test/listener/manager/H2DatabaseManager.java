package com.dictapp.test.listener.manager;

import lombok.extern.log4j.Log4j;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Log4j
public class H2DatabaseManager extends AbstractDatabaseManager {

    public H2DatabaseManager(JdbcTemplate jdbcTemplate, String schema) {
        super(jdbcTemplate, schema);
    }

    @Override
    public void truncateAllTables() {
        jdbcTemplate.execute("SET REFERENTIAL_INTEGRITY FALSE");

        List<String> tableNames = jdbcTemplate.query("SHOW TABLES", (rs, rowNum) -> rs.getString("TABLE_NAME"));
        truncateTablesByName(tableNames);

        jdbcTemplate.execute("SET REFERENTIAL_INTEGRITY TRUE");
    }

    @Override
    public void truncateTablesByName(Collection<String> tableNames) {
        List<String> tablesToTruncate = tableNames.stream()
                .filter(tableName -> !tableName.equalsIgnoreCase(CHANGELOG_TABLE_NAME))
                .collect(Collectors.toList());

        log.info("Truncate H2 tables: " + tablesToTruncate.size() + " size");
        tablesToTruncate.forEach(tableName -> jdbcTemplate.execute("TRUNCATE TABLE " + tableName));
    }
}
