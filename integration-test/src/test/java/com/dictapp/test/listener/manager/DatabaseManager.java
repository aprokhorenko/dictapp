package com.dictapp.test.listener.manager;

import java.util.Collection;

public interface DatabaseManager {

    void runScripts(Collection<String> changeSetLocations, String dirName) throws Exception;

    void truncateAllTables();
}
