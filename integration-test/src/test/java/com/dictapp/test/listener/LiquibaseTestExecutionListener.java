package com.dictapp.test.listener;

import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.listener.manager.DatabaseManager;
import com.dictapp.test.listener.manager.H2DatabaseManager;
import com.dictapp.test.listener.manager.PostgresDatabaseManager;
import lombok.extern.log4j.Log4j;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

import javax.sql.DataSource;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Log4j
public class LiquibaseTestExecutionListener extends AbstractTestExecutionListener {

    private static final String TEST_DATABASE_SCRIPT_DIR_NAME = "/database/";
    private static final String DRIVER_CLASS_PROPERTY = "db.driverClassName";
    private static final String DB_SCHEMA_PROPERTY = "db.schema";

    private static DatabaseManager databaseManager;

    @Override
    public void beforeTestClass(TestContext testContext) throws Exception {
        if (Objects.isNull(databaseManager)) {
            initDataBaseManager(testContext);
        }

        databaseManager.truncateAllTables();
    }

    @Override
    public void beforeTestMethod(TestContext testContext) throws Exception {
        List<String> liquibaseScripts = getLiquibaseScriptLocationFrom(testContext.getTestClass(), testContext.getTestMethod());
        databaseManager.runScripts(liquibaseScripts, TEST_DATABASE_SCRIPT_DIR_NAME);
    }

    @Override
    public void afterTestMethod(TestContext testContext) throws Exception {
        databaseManager.truncateAllTables();
    }

    private List<String> getLiquibaseScriptLocationFrom(Class<?> testClass, Method testMethod) throws Exception {
        LinkedList<String> liquibaseScriptLocation = new LinkedList<>();

        LiquibaseChangeSet methodChangeSet = Objects.nonNull(testMethod)
                ? testMethod.getAnnotation(LiquibaseChangeSet.class)
                : null;

        if (Objects.isNull(methodChangeSet) || methodChangeSet.includeTopChangeSet()) {
            List<LiquibaseChangeSet> classChangeSetAnnotations = getLiquibaseChangeSetAnnotations(testClass);
            for (LiquibaseChangeSet liquibaseChangeSet : classChangeSetAnnotations) {
                Collections.addAll(liquibaseScriptLocation, liquibaseChangeSet.value());
            }
        }

        if (Objects.nonNull(methodChangeSet)) {
            for (String scriptLocation : methodChangeSet.value()) {
                liquibaseScriptLocation.addLast(scriptLocation);
            }
        }

        return liquibaseScriptLocation;
    }

    /**
     * Returns all {@link LiquibaseChangeSet} from the class and from the superclasses
     * before the first match {@link LiquibaseChangeSet#includeTopChangeSet()} as {@literal false}.
     * If there are no annotations in the classes, an empty collection will be returned
     *
     * @param clazz
     * @return list of {@link LiquibaseChangeSet}
     */
    private List<LiquibaseChangeSet> getLiquibaseChangeSetAnnotations(Class<?> clazz) {
        LinkedList<LiquibaseChangeSet> annotations = new LinkedList<>();

        Class<?> superclass = clazz.getSuperclass();
        LiquibaseChangeSet liquibaseChangeSet = clazz.getAnnotation(LiquibaseChangeSet.class);
        if (Objects.nonNull(liquibaseChangeSet)) {
            if (liquibaseChangeSet.includeTopChangeSet() && Objects.nonNull(superclass)) {
                annotations.addAll(getLiquibaseChangeSetAnnotations(superclass));
            }
            annotations.addLast(liquibaseChangeSet);
        } else if (Objects.nonNull(superclass)) {
            annotations.addAll(getLiquibaseChangeSetAnnotations(superclass));
        }

        return annotations;
    }

    private DatabaseManager constructDatabaseManager(DBMSType dbmsType, JdbcTemplate jdbcTemplate, String schema) throws SQLException {
        switch (dbmsType) {
            case H2:
                return new H2DatabaseManager(jdbcTemplate, schema);
            case POSTGRES:
                return new PostgresDatabaseManager(jdbcTemplate, schema);
            default:
                throw new IllegalArgumentException("Unsupported dbms!");
        }
    }

    private void initDataBaseManager(TestContext testContext) throws SQLException {
        DataSource dataSource = testContext.getApplicationContext().getBean(DataSource.class);
        ApplicationContext applicationContext = testContext.getApplicationContext();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Environment environment = applicationContext.getEnvironment();
        String dbSchema = environment.getProperty(DB_SCHEMA_PROPERTY);

        String driverClass = environment.getProperty(DRIVER_CLASS_PROPERTY);
        DBMSType dbmsType = DBMSType.findBy(driverClass)
                .orElseThrow(() -> new RuntimeException("Driver class not supported! " + driverClass));

        databaseManager = constructDatabaseManager(dbmsType, jdbcTemplate, dbSchema);
    }
}
