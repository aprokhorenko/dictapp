package com.dictapp.test.listener;

import com.dictapp.pojo.auth.AuthResponse;
import com.dictapp.test.annotation.Authorize;
import com.dictapp.test.helper.SessionTokenHolder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;
import static com.dictapp.security.filter.JwtAuthFilter.TOKEN;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthTestExecutionListener extends AbstractTestExecutionListener {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public void beforeTestMethod(TestContext testContext) throws Exception {
        Authorize authorize = testContext.getTestMethod().getAnnotation(Authorize.class);
        if (Objects.nonNull(authorize)) {
            ApplicationContext applicationContext = testContext.getApplicationContext();

            String facebookToken = authorize.facebookToken();

            MockMvc mvc = applicationContext.getBean(MockMvc.class);

            String path = API_V1_BASE_PATH + "auth/facebook";
            AtomicReference<String> token = new AtomicReference<>();
            mvc.perform(get(path).servletPath(path)
                    .contentType(MediaType.APPLICATION_JSON)
                    .param(TOKEN, facebookToken))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(result -> {
                        AuthResponse authResponse = OBJECT_MAPPER.readValue(result.getResponse().getContentAsString(), AuthResponse.class);
                        token.set(authResponse.getToken());
                    });

            String tokenAsString = token.get();
            if (Objects.nonNull(tokenAsString)) {
                SessionTokenHolder sessionTokenHolder = applicationContext.getBean(SessionTokenHolder.class);
                sessionTokenHolder.setCurrentToken(tokenAsString);
            } else {
                throw new AssertionError("");
            }
        }
    }
}
