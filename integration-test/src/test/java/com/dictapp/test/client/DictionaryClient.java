package com.dictapp.test.client;

import com.dictapp.dto.DictionaryDto;
import com.dictapp.test.client.base.AbstractWebClient;
import com.dictapp.test.helper.SessionTokenHolder;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
public class DictionaryClient extends AbstractWebClient {

    private static final String BASE_DICTIONARY_PATH = API_V1_BASE_PATH + "dictionary";

    public DictionaryClient(SessionTokenHolder sessionTokenHolder, MockMvc mockMvc) {
        super(sessionTokenHolder, mockMvc);
    }

    public Page<DictionaryDto> getAll(Integer page, Integer size) throws Exception {
        String path = BASE_DICTIONARY_PATH + "/all";
        String json = jsonBuilder()
                .put("page", page)
                .put("size", size)
                .build();
        String responseJson = mvc.perform(get(path).servletPath(path)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader(sessionTokenHolder.getCurrentToken()))
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        return parseJsonAsPage(responseJson, DictionaryDto.class);
    }


    public List<DictionaryDto> createOrUpdate(List<DictionaryDto> dictionaries) throws Exception {
        String json = jsonBuilder()
                .putPojo("dictionaries", dictionaries)
                .build();
        String responseJson = mvc.perform(post(BASE_DICTIONARY_PATH).servletPath(BASE_DICTIONARY_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader(sessionTokenHolder.getCurrentToken()))
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        return MAPPER.readValue(responseJson, new TypeReference<List<DictionaryDto>>() {
        });
    }

    public int delete(List<String> ids) throws Exception {
        String json = jsonBuilder()
                .putPojo("ids", ids)
                .build();
        String response = mvc.perform(MockMvcRequestBuilders.delete(BASE_DICTIONARY_PATH).servletPath(BASE_DICTIONARY_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader(sessionTokenHolder.getCurrentToken()))
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        return new Integer(response);
    }
}
