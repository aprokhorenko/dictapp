package com.dictapp.test.client.base;

import com.dictapp.test.helper.SessionTokenHolder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractWebClient {
    protected static final String BEARER = "Bearer";

    protected static final ObjectMapper MAPPER = new ObjectMapper()
            .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    protected final SessionTokenHolder sessionTokenHolder;
    protected final MockMvc mvc;

    public AbstractWebClient(SessionTokenHolder sessionTokenHolder, MockMvc mockMvc) {
        this.sessionTokenHolder = sessionTokenHolder;
        this.mvc = mockMvc;
    }

    protected JsonBuilder jsonBuilder() {
        return new JsonBuilder();
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    protected static class JsonBuilder {
        ObjectNode rootNode = JsonNodeFactory.instance.objectNode();

        public JsonBuilder putPojo(String key, Collection<?> objects) {
            ArrayNode childNodes = JsonNodeFactory.instance.arrayNode();

            childNodes.addAll(objects.stream()
                    .map(JsonNodeFactory.instance::pojoNode)
                    .collect(Collectors.toList())
            );

            rootNode.set(key, childNodes);
            return this;
        }

        public JsonBuilder putPojo(String key, Object object) {
            rootNode.set(key, JsonNodeFactory.instance.pojoNode(object));
            return this;
        }

        public JsonBuilder put(String key, String val) {
            rootNode.put(key, val);
            return this;
        }

        public JsonBuilder put(String key, Integer val) {
            rootNode.put(key, val);
            return this;
        }

        public JsonBuilder put(String key, Long val) {
            rootNode.put(key, val);
            return this;
        }

        public String build() throws JsonProcessingException {
            return MAPPER.writeValueAsString(rootNode);
        }
    }

    protected <T> Page<T> parseJsonAsPage(String json, Class<T> objType) throws IOException {
        JsonNode rootNode = MAPPER.readTree(json);

        JsonNode content = rootNode.get("content");
        List<T> objects = MAPPER.readValue(content.toString(), MAPPER.getTypeFactory().constructCollectionType(List.class, objType));

        int size = rootNode.get("size").asInt();
        int totalElements = rootNode.get("totalElements").asInt();
        int page = rootNode.get("number").asInt();

        Pageable pageRequest = new PageRequest(page, size);
        return new PageImpl<>(objects, pageRequest, totalElements);
    }

    protected String getAuthorizationHeader(String token) {
        return BEARER + " " + token;
    }
}
