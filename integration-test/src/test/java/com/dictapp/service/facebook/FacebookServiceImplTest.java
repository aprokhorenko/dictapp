package com.dictapp.service.facebook;

import com.dictapp.ApplicationConstants;
import com.dictapp.exception.ExceptionMessages;
import com.dictapp.exception.facebook.FacebookAuthException;
import com.dictapp.persistance.FacebookData;
import com.dictapp.pojo.facebook.FacebookUser;
import com.dictapp.repository.facebook.FacebookRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Objects;

@AllArgsConstructor
@Service
@Profile(ApplicationConstants.PROFILE_TEST)
public class FacebookServiceImplTest implements FacebookService {

    private final FacebookRepository facebookRepository;

    @Override
    public FacebookUser loadUser(String accessToken) throws FacebookAuthException {
        FacebookData facebookData = facebookRepository.findByToken(accessToken);
        if (Objects.isNull(facebookData)) {
            throw new FacebookAuthException(ExceptionMessages.FACEBOOK_WRONG_AUTH_MESSAGE);
        }
        return new FacebookUser(facebookData.getFacebookId());
    }
}
