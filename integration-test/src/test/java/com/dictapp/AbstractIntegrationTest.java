package com.dictapp;

import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.helper.SessionTokenHolder;
import com.dictapp.test.listener.AuthTestExecutionListener;
import com.dictapp.test.listener.LiquibaseTestExecutionListener;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@RunWith(value = SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = WebAppStarter.class)
@AutoConfigureMockMvc
@TestExecutionListeners( {LiquibaseTestExecutionListener.class, DependencyInjectionTestExecutionListener.class, AuthTestExecutionListener.class})
@TestPropertySource(locations = {
        "classpath:application-test.properties"
})
@ActiveProfiles(ApplicationConstants.PROFILE_TEST)
@LiquibaseChangeSet(value = {
        "base/facebook-data.xml",
        "base/user-data.xml"
})
public abstract class AbstractIntegrationTest {

    @Autowired
    protected SessionTokenHolder sessionTokenHolder;
}
