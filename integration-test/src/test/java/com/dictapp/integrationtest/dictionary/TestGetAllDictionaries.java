package com.dictapp.integrationtest.dictionary;

import com.dictapp.AbstractIntegrationTest;
import com.dictapp.dto.DictionaryDto;
import com.dictapp.dto.base.BaseDto;
import com.dictapp.test.annotation.Authorize;
import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.client.DictionaryClient;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

@LiquibaseChangeSet(value = "TestGetAllDictionaries/create-three-dictionaries.xml")
public class TestGetAllDictionaries extends AbstractIntegrationTest {

    @Autowired
    private DictionaryClient dictionaryClient;

    @Test
    @Authorize
    public void shouldReturnTwoDictionariesByUser1() throws Exception {
        Page<DictionaryDto> dictionaries = dictionaryClient.getAll(null, null);

        Assert.assertThat(dictionaries.getTotalElements(), is(2L));

        String[] expectedDictionaryIds = {"1", "3"};
        Set<String> actualDictionaryIds = dictionaries.getContent().stream()
                .map(BaseDto::getId)
                .collect(Collectors.toSet());

        Assert.assertThat(actualDictionaryIds, hasItems(expectedDictionaryIds));
    }

    @Test
    @Authorize(facebookToken = "2")
    public void shouldReturnOneDictionaryByUser2() throws Exception {
        Page<DictionaryDto> dictionaries = dictionaryClient.getAll(null, null);

        Assert.assertThat(dictionaries.getTotalElements(), is(1L));

        List<DictionaryDto> content = dictionaries.getContent();

        DictionaryDto dictionaryDto = content.get(0);
        Assert.assertThat(dictionaryDto.getId(), is("2"));
        Assert.assertThat(dictionaryDto.getUserId(), is("user2"));
    }
}
