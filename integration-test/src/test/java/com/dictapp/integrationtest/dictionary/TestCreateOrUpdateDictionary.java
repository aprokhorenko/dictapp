package com.dictapp.integrationtest.dictionary;

import com.dictapp.AbstractIntegrationTest;
import com.dictapp.dto.DictionaryDto;
import com.dictapp.test.annotation.Authorize;
import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.client.DictionaryClient;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

public class TestCreateOrUpdateDictionary extends AbstractIntegrationTest {

    @Autowired
    private DictionaryClient dictionaryClient;

    @Test
    @Authorize
    public void testCreateDictionaries() throws Exception {
        DictionaryDto dictionary = new DictionaryDto()
                .setName("dict 1")
                .setStudyLang("en")
                .setClientUUID("12")
                .setTranslationLang("en");

        List<DictionaryDto> savedDictionaries = dictionaryClient.createOrUpdate(Collections.singletonList(dictionary));
        Assert.assertThat(savedDictionaries.size(), is(1));

        DictionaryDto savedDictionary = savedDictionaries.get(0);
        Assert.assertThat(dictionary.getName(), is(savedDictionary.getName()));
        Assert.assertThat(dictionary.getStudyLang(), is(savedDictionary.getStudyLang()));
        Assert.assertThat(dictionary.getClientUUID(), is(savedDictionary.getClientUUID()));
        Assert.assertThat(dictionary.getTranslationLang(), is(savedDictionary.getTranslationLang()));
    }

    @Test
    @Authorize
    @LiquibaseChangeSet(value = "/TestCreateOrUpdateDictionary/create-dictionaries.xml")
    public void testUpdateDictionary() throws Exception {
        Page<DictionaryDto> dictionariesPage = dictionaryClient.getAll(null, null);
        Assert.assertThat(dictionariesPage.getTotalElements(), is(2L));

        List<DictionaryDto> content = dictionariesPage.getContent();
        DictionaryDto toUpdateDictionary = content.get(0);
        toUpdateDictionary.setName("___00____00__");
        toUpdateDictionary.setTranslationLang("ru");

        List<DictionaryDto> savedDictionaries = dictionaryClient.createOrUpdate(content);
        Assert.assertThat(savedDictionaries.size(), is(content.size()));
        Assert.assertThat(savedDictionaries, hasItems(toUpdateDictionary));
    }

    @Test
    @Authorize
    @LiquibaseChangeSet(value = "TestCreateOrUpdateDictionary/create-dictionaries.xml")
    public void testCreateAndUpdateDictionaries() throws Exception {
        Page<DictionaryDto> dictionariesPage = dictionaryClient.getAll(null, null);
        Assert.assertThat(dictionariesPage.getTotalElements(), is(2L));

        List<DictionaryDto> toUpdateDictionaries = new ArrayList<>(dictionariesPage.getContent());

        DictionaryDto dictionaryDto = new DictionaryDto()
                .setName("ololo")
                .setStudyLang("ru")
                .setClientUUID("5")
                .setTranslationLang("ru");
        toUpdateDictionaries.add(dictionaryDto);

        List<DictionaryDto> savedDictionaries = dictionaryClient.createOrUpdate(toUpdateDictionaries);
        Assert.assertThat(savedDictionaries.size(), is(toUpdateDictionaries.size()));
        Assert.assertThat(savedDictionaries, hasItems(dictionariesPage.getContent().toArray(new DictionaryDto[dictionariesPage.getContent().size()])));

        Set<String> dictionariesClientUUIDs = savedDictionaries.stream()
                .map(DictionaryDto::getClientUUID)
                .collect(Collectors.toSet());
        Assert.assertThat(dictionariesClientUUIDs, hasItems(dictionaryDto.getClientUUID()));

        dictionariesPage  = dictionaryClient.getAll(null, null);
        Assert.assertThat(dictionariesPage.getTotalElements(), is(3L));
    }
}
