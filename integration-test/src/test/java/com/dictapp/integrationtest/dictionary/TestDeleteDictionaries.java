package com.dictapp.integrationtest.dictionary;

import com.dictapp.AbstractIntegrationTest;
import com.dictapp.dto.DictionaryDto;
import com.dictapp.dto.WordDto;
import com.dictapp.test.annotation.Authorize;
import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.client.DictionaryClient;
import com.dictapp.test.client.WordClient;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;

public class TestDeleteDictionaries extends AbstractIntegrationTest {

    @Autowired
    private WordClient wordClient;
    @Autowired
    private DictionaryClient dictionaryClient;

    @Test
    @Authorize
    @LiquibaseChangeSet("TestDeleteDictionaries/dictionary-to-delete.xml")
    public void shouldDeleteDictionaryWithWords() throws Exception {
        Page<WordDto> words = wordClient.getAll(null, null);
        Assert.assertThat(words.getTotalElements(), is(2L));

        Page<DictionaryDto> dictionaries = dictionaryClient.getAll(null, null);
        Assert.assertThat(dictionaries.getTotalElements(), is(1L));

        DictionaryDto dictionaryToDelete = dictionaries.getContent().get(0);
        int delete = dictionaryClient.delete(Collections.singletonList(dictionaryToDelete.getId()));
        Assert.assertThat(delete, is(1));

        words = wordClient.getAll(null, null);
        Assert.assertThat(words.getTotalElements(), is(0L));

        dictionaries = dictionaryClient.getAll(null, null);
        Assert.assertThat(dictionaries.getTotalElements(), is(0L));
    }
}
