package com.dictapp.integrationtest.word;

import com.dictapp.AbstractIntegrationTest;
import com.dictapp.dto.WordDto;
import com.dictapp.test.annotation.Authorize;
import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.client.WordClient;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;

@LiquibaseChangeSet(value = "TestCreateWords/init.xml")
public class TestCreateWords extends AbstractIntegrationTest {

    @Autowired
    private WordClient wordClient;

    @Test
    @Authorize
    public void shouldCreateTwoWords() throws Exception {
        String testWord = "test";
        WordDto word1 = new WordDto()
                .setWord(testWord)
                .setClientUUID(UUID.randomUUID().toString())
                .setStudyStatus("NEW")
                .setUserId("user2")
                .setTranscription("тест")
                .setDictionaryId("1");
        WordDto word2 = new WordDto()
                .setWord("test1")
                .setClientUUID(UUID.randomUUID().toString())
                .setStudyStatus("NEW")
                .setUserId("user3")
                .setTranscription("тест1")
                .setDictionaryId("1");
        List<WordDto> wordsToSave = Arrays.asList(word1, word2);

        List<WordDto> savedWords = wordClient.createOrUpdateWords(wordsToSave);

        Assert.assertThat(savedWords.size(), is(wordsToSave.size()));

        boolean hasTestWordInSavedObjects = savedWords.stream()
                .anyMatch(word -> Objects.equals(word.getWord(), testWord));
        Assert.assertTrue(hasTestWordInSavedObjects);

        String currentUserId = "user1";
        boolean allWordsFromCurrentUser = savedWords.stream()
                .allMatch(word -> Objects.equals(word.getUserId(), currentUserId));
        Assert.assertTrue(allWordsFromCurrentUser);
    }
}
