package com.dictapp.integrationtest.word;

import com.dictapp.AbstractIntegrationTest;
import com.dictapp.dto.WordDto;
import com.dictapp.dto.base.BaseDto;
import com.dictapp.test.annotation.Authorize;
import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.client.WordClient;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;

public class TestDeleteWords extends AbstractIntegrationTest {

    @Autowired
    private WordClient wordClient;

    @Test
    @Authorize
    @LiquibaseChangeSet("TestDeleteWords/create-words-to-delete.xml")
    public void testDeleteWord() throws Exception {
        Page<WordDto> words = wordClient.getAll(null, null);
        Assert.assertThat(words.getTotalElements(), is(3L));

        Set<String> ids = words.getContent().stream()
                .map(BaseDto::getId)
                .collect(Collectors.toSet());
        int delete = wordClient.delete(ids);
        Assert.assertThat(delete, is(3));

        words = wordClient.getAll(null, null);
        Assert.assertThat(words.getTotalElements(), is(0L));
    }
}
