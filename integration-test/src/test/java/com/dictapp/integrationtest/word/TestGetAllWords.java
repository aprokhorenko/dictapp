package com.dictapp.integrationtest.word;

import com.dictapp.AbstractIntegrationTest;
import com.dictapp.dto.WordDto;
import com.dictapp.test.annotation.Authorize;
import com.dictapp.test.annotation.LiquibaseChangeSet;
import com.dictapp.test.client.WordClient;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

public class TestGetAllWords extends AbstractIntegrationTest {

    @Autowired
    private WordClient wordClient;

    @Test
    @Authorize
    @LiquibaseChangeSet("TestGetAllWords/create-three-words.xml")
    public void shouldReturnTwoWordsByUser1() throws Exception {
        Page<WordDto> words = wordClient.getAll(null, null);
        Assert.assertThat(words.getTotalElements(), is(2L));
        Set<String> wordIds = words.getContent().stream()
                .map(WordDto::getId)
                .collect(Collectors.toSet());
        Assert.assertThat(wordIds, hasItems("1", "2"));
    }

    @Test
    @Authorize(facebookToken = "2")
    @LiquibaseChangeSet("TestGetAllWords/create-three-words.xml")
    public void shouldReturnOneWordByUser2() throws Exception {
        Page<WordDto> words = wordClient.getAll(null, null);
        Assert.assertThat(words.getTotalElements(), is(1L));
        Assert.assertThat(words.getContent().get(0).getId(), is("3"));
    }
}
