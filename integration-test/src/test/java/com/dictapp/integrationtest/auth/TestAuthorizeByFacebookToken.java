package com.dictapp.integrationtest.auth;

import com.dictapp.AbstractIntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.concurrent.atomic.AtomicReference;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;
import static com.dictapp.security.filter.JwtAuthFilter.TOKEN;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestAuthorizeByFacebookToken extends AbstractIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void testAuthByFacebookToken() throws Exception {
        String path = API_V1_BASE_PATH + "auth/facebook";
        AtomicReference<String> token = new AtomicReference<>();
        mvc.perform(get(path).servletPath(path)
                .contentType(MediaType.APPLICATION_JSON)
                .param(TOKEN, "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    token.set(result.getResponse().getContentAsString());
                });
    }

    @Test
    public void testShouldThrowExceptionWithWrongToken() throws Exception {
        String path = API_V1_BASE_PATH + "auth/facebook";
        mvc.perform(get(path).servletPath(path)
                .contentType(MediaType.APPLICATION_JSON)
                .param(TOKEN, "1000000000000"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Wrong credential!"));
    }
}
