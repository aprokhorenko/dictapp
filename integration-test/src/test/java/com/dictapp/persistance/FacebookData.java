package com.dictapp.persistance;

import com.dictapp.persistance.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "facebook_test_data")
@Getter
@Setter
public class FacebookData extends BaseEntity {

    @Column(name = "token")
    private String token;

    @Column(name = "facebook_id")
    private String facebookId;
}
