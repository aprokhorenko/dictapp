package com.dictapp.repository.facebook;

import com.dictapp.persistance.FacebookData;
import org.springframework.data.repository.CrudRepository;

public interface FacebookRepository extends CrudRepository<FacebookData, String> {

    FacebookData findByToken(String token);
}
