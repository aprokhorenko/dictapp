package com.dictapp.service.facebook;

import com.dictapp.ApplicationConstants;
import com.dictapp.exception.ExceptionMessages;
import com.dictapp.exception.facebook.FacebookAuthException;
import com.dictapp.pojo.facebook.FacebookUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.social.ApiException;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

@Service
@Profile( {ApplicationConstants.PROFILE_DEV, ApplicationConstants.PROFILE_PROD})
public class FacebookServiceImpl implements FacebookService {

    private final String facebookAppId;

    public FacebookServiceImpl(@Value("${app.facebook.appId}") String facebookAppId) {
        this.facebookAppId = facebookAppId;
    }

    @Override
    public FacebookUser loadUser(String accessToken) {
        try {
            FacebookTemplate facebookTemplate = new FacebookTemplate(accessToken, null, facebookAppId);
            User userProfile = facebookTemplate.userOperations().getUserProfile();

            return new FacebookUser(userProfile.getId());
        } catch (ApiException e) {
            throw new FacebookAuthException(ExceptionMessages.FACEBOOK_WRONG_AUTH_MESSAGE);
        }
    }
}
