package com.dictapp.service.facebook;

import com.dictapp.exception.facebook.FacebookAuthException;
import com.dictapp.pojo.facebook.FacebookUser;

public interface FacebookService {

    FacebookUser loadUser(String accessToken) throws FacebookAuthException;
}
