package com.dictapp.service.tag;

import com.dictapp.persistance.TagEntity;
import com.dictapp.persistance.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserTagService {

    Page<TagEntity> findAll(Pageable pageable, UserEntity user);
}
