package com.dictapp.service.tag;

import com.dictapp.persistance.TagEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.repository.tag.UserTagRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class UserTagServiceImpl implements UserTagService {

    private final UserTagRepository userTagRepository;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Page<TagEntity> findAll(Pageable pageable, UserEntity user) {
        return userTagRepository.findAllByUser(pageable, user);
    }
}
