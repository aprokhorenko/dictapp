package com.dictapp.service.auth;

import com.dictapp.persistance.auth.SessionEntity;

import java.util.Optional;

public interface AuthService {

    SessionEntity authByFacebookToken(String accessToken);

    Optional<SessionEntity> getSessionByToken(String token);
}
