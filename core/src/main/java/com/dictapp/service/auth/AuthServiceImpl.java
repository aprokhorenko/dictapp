package com.dictapp.service.auth;

import com.dictapp.component.token.AuthTokenGenerator;
import com.dictapp.persistance.auth.SessionEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.pojo.facebook.FacebookUser;
import com.dictapp.repository.auth.SessionRepository;
import com.dictapp.repository.user.UserRepository;
import com.dictapp.service.facebook.FacebookService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class AuthServiceImpl implements AuthService {

    private final FacebookService facebookService;
    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;
    private final AuthTokenGenerator authTokenGenerator;

    public AuthServiceImpl(FacebookService facebookService,
                           UserRepository userRepository,
                           SessionRepository sessionRepository,
                           AuthTokenGenerator authTokenGenerator) {
        this.facebookService = facebookService;
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
        this.authTokenGenerator = authTokenGenerator;
    }

    @Override
    public SessionEntity authByFacebookToken(String accessToken) {
        FacebookUser facebookUser = facebookService.loadUser(accessToken);

        Optional<UserEntity> userOpt = userRepository.findUserEntityByFacebookId(facebookUser.getId());
        final UserEntity user;
        if (userOpt.isPresent()) {
            user = userOpt.get();
        } else {
            UserEntity convertedUser = convertToUserEntity(facebookUser);
            user = userRepository.save(convertedUser);
        }

        SessionEntity newSession = createNewSession(user);
        return sessionRepository.save(newSession);
    }

    @Override
    public Optional<SessionEntity> getSessionByToken(String token) {
        return sessionRepository.findSessionEntityByToken(token);
    }

    private UserEntity convertToUserEntity(FacebookUser facebookUser) {
        UserEntity user = new UserEntity();
        user.setFacebookId(facebookUser.getId());
        return user;
    }

    private SessionEntity createNewSession(UserEntity user) {
        SessionEntity session = new SessionEntity();
        session.setToken(authTokenGenerator.generateToken(user.getId()));
        session.setUser(user);
        return session;
    }
}
