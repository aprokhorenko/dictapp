package com.dictapp.service.dictionary;

import com.dictapp.persistance.DictionaryEntity;
import com.dictapp.persistance.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DictionaryService {

    Page<DictionaryEntity> findAll(Pageable pageable, UserEntity user);

    List<DictionaryEntity> createOrUpdate(List<DictionaryEntity> dictionaries, UserEntity user);

    int delete(List<String> ids, UserEntity user);
}
