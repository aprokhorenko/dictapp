package com.dictapp.service.dictionary;

import com.dictapp.persistance.DictionaryEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.repository.dictionary.DictionaryRepository;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Transactional
@Service
@AllArgsConstructor
public class DictionaryServiceImpl implements DictionaryService {

    private static final int DICTIONARY_BATCH_SIZE = 1000;

    private final DictionaryRepository dictionaryRepository;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Page<DictionaryEntity> findAll(Pageable pageable, UserEntity user) {
        return dictionaryRepository.findAllByUser(pageable, user);
    }

    @Override
    public List<DictionaryEntity> createOrUpdate(List<DictionaryEntity> dictionaries, UserEntity user) {
        Set<String> dictionaryClientUUID = dictionaries.stream()
                .map(DictionaryEntity::getClientUUID)
                .collect(Collectors.toSet());

        Map<String, DictionaryEntity> dictionaryClientUUIDMap = dictionaryRepository.findAllByClientUUIDInAndUser(dictionaryClientUUID, user).stream()
                .collect(Collectors.toMap(DictionaryEntity::getClientUUID, Function.identity()));
        if (!dictionaryClientUUIDMap.isEmpty()) {
            return dictionaries.stream()
                    .map(dict -> {
                        DictionaryEntity oldDict = dictionaryClientUUIDMap.get(dict.getClientUUID());
                        return Objects.nonNull(oldDict) ? updateDictionary(oldDict, dict) : createDictionary(dict, user);
                    })
                    .collect(Collectors.toList());
        } else {
            return createDictionaries(dictionaries, user);
        }
    }

    @Override
    public int delete(List<String> dictionariesIds, UserEntity user) {
        return Lists.partition(dictionariesIds, DICTIONARY_BATCH_SIZE).stream()
                .mapToInt(ids -> dictionaryRepository.deleteDictionariesByIds(ids, user))
                .sum();
    }

    private List<DictionaryEntity> createDictionaries(List<DictionaryEntity> dictionaries, UserEntity user) {
        dictionaries.forEach(dictionaryEntity -> dictionaryEntity.setUser(user));
        return Lists.newArrayList(dictionaryRepository.save(dictionaries));
    }

    private DictionaryEntity createDictionary(DictionaryEntity dictionary, UserEntity user) {
        dictionary.setId(null);
        dictionary.setUser(user);
        return dictionaryRepository.save(dictionary);
    }

    private DictionaryEntity updateDictionary(DictionaryEntity oldDict, DictionaryEntity newDict) {
        oldDict.setName(newDict.getName());
        oldDict.setStudyLang(newDict.getStudyLang());
        oldDict.setTranslationLang(newDict.getTranslationLang());
        return dictionaryRepository.save(oldDict);
    }
}
