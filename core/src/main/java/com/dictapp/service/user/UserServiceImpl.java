package com.dictapp.service.user;

import com.dictapp.persistance.user.UserEntity;
import com.dictapp.repository.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserEntity findById(String id) {
        return userRepository.findOne(id);
    }
}
