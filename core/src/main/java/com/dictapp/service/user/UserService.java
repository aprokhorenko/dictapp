package com.dictapp.service.user;

import com.dictapp.persistance.user.UserEntity;

public interface UserService {

    UserEntity findById(String id);
}
