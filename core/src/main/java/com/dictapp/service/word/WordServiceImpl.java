package com.dictapp.service.word;

import com.dictapp.persistance.WordEntity;
import com.dictapp.persistance.base.BaseEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.repository.dictionary.DictionaryRepository;
import com.dictapp.repository.word.WordRepository;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
@Transactional
public class WordServiceImpl implements WordService {

    private static final int WORD_BATCH_SIZE = 1000;

    private final WordRepository wordRepository;
    private final DictionaryRepository dictionaryRepository;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Page<WordEntity> findAll(Pageable pageable, UserEntity user) {
        return wordRepository.findAllByUserAndDeletedFalse(pageable, user);
    }

    @Override
    public List<WordEntity> createOrUpdateWords(Collection<WordEntity> wordEntities, UserEntity user) {
        Set<String> wordClientUUID = wordEntities.stream()
                .map(WordEntity::getClientUUID)
                .collect(Collectors.toSet());

        Map<String, WordEntity> wordEntitiesByUUID = wordRepository.findAllByClientUUIDInAndUser(wordClientUUID, user).stream()
                .collect(Collectors.toMap(WordEntity::getClientUUID, Function.identity()));
        if (!wordEntitiesByUUID.isEmpty()) {
            Map<Boolean, List<WordEntity>> wordMap = wordEntities.stream()
                    .collect(Collectors.partitioningBy(word -> wordEntitiesByUUID.containsKey(word.getClientUUID())));

            List<WordEntity> updatedWords = wordMap.getOrDefault(Boolean.TRUE, Collections.emptyList()).stream()
                    .map(word -> {
                        WordEntity oldWord = wordEntitiesByUUID.get(word.getClientUUID());
                        return updateWord(oldWord, word);
                    })
                    .collect(Collectors.toList());

            List<WordEntity> createdWords = createWords(wordMap.getOrDefault(Boolean.FALSE, Collections.emptyList()), user);
            return Stream.of(createdWords, updatedWords)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
        } else {
            return createWords(wordEntities, user);
        }
    }

    @Override
    public int delete(Collection<String> wordIds, UserEntity user) {
        return Lists.partition(new ArrayList<>(wordIds), WORD_BATCH_SIZE)
                .stream()
                .mapToInt(ids -> wordRepository.deleteWordsByIds(ids, user))
                .sum();
    }

    private WordEntity updateWord(WordEntity oldWord, WordEntity newWord) {
        oldWord.setWord(newWord.getWord());
        oldWord.setFavorite(newWord.isFavorite());
        oldWord.setDictionary(newWord.getDictionary());
        oldWord.setTranscription(newWord.getTranscription());
        oldWord.setTranslation(newWord.getTranslation());
        oldWord.setStudyStatus(newWord.getStudyStatus());
        oldWord.setRating(newWord.getRating());
        return wordRepository.save(oldWord);
    }

    private List<WordEntity> createWords(Collection<WordEntity> wordEntities, UserEntity user) {
        if (!wordEntities.isEmpty()) {
            verifyThatWordInUserDictionary(wordEntities, user);

            wordEntities.forEach(word -> {
                word.setId(null);
                word.setUser(user);
            });

            Iterable<WordEntity> savedWords = wordRepository.save(wordEntities);
            return Lists.newArrayList(savedWords);
        } else {
            return Collections.emptyList();
        }
    }

    private void verifyThatWordInUserDictionary(Collection<WordEntity> words, UserEntity user) {
        Set<String> dictionaryIds = words.stream()
                .map(WordEntity::getDictionary)
                .filter(Objects::nonNull)
                .map(BaseEntity::getId)
                .collect(Collectors.toSet());

        int dictionariesCount = dictionaryRepository.getCountDictionariesByUser(dictionaryIds, user.getId());
        if (dictionariesCount != dictionaryIds.size()) {
            throw new IllegalArgumentException("User hasn't some dictionaries! Check input param.");
        }
    }
}
