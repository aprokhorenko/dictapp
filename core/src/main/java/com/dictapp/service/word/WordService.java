package com.dictapp.service.word;

import com.dictapp.persistance.WordEntity;
import com.dictapp.persistance.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface WordService {

    Page<WordEntity> findAll(Pageable pageable, UserEntity user);

    List<WordEntity> createOrUpdateWords(Collection<WordEntity> wordEntities, UserEntity user);

    int delete(Collection<String> ids, UserEntity user);
}
