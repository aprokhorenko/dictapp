package com.dictapp;

public interface ApplicationConstants {

    String API_V1_BASE_PATH = "/api/v1/";

    String PROFILE_DEV = "dev";
    String PROFILE_PROD = "prod";
    String PROFILE_TEST = "test";
}
