package com.dictapp.persistance.auth;

import com.dictapp.persistance.base.BaseEntity;
import com.dictapp.persistance.user.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "user_session")
@Getter
@Setter
public class SessionEntity extends BaseEntity {

    @Column(name = "create_date", updatable = false)
    @CreatedDate
    private Instant createDate;

    @Column(name = "token")
    private String token;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;
}
