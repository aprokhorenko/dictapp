package com.dictapp.persistance.user;

import com.dictapp.persistance.TagEntity;
import com.dictapp.persistance.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "users")
@Getter
@Setter
public class UserEntity extends BaseEntity {

    @Column(name = "create_date", updatable = false)
    @CreatedDate
    private Instant createDate;

    @Column(name = "facebook_id", nullable = false)
    private String facebookId;

    @OneToMany(mappedBy = "user")
    private List<TagEntity> tags;
}
