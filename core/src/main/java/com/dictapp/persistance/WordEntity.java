package com.dictapp.persistance;

import com.dictapp.persistance.base.BaseEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.pojo.word.StudyStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "word")
@Getter
@Setter
public class WordEntity extends BaseEntity {

    @Column(name = "create_date", updatable = false)
    @CreatedDate
    private Instant createDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "word")
    private String word;

    @Column(name = "client_uuid")
    private String clientUUID;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "deleted")
    private boolean deleted;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "favorite")
    private boolean favorite;

    @ManyToOne
    @JoinColumn(name = "dictionary_id")
    private DictionaryEntity dictionary;

    @Column(name = "transcription")
    private String transcription;

    @Column(name = "translation")
    private String translation;

    @Enumerated(EnumType.STRING)
    @Column(name = "study_status")
    private StudyStatus studyStatus;

    @Column(name = "rating")
    private int rating;
}
