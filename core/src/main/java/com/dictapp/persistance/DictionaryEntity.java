package com.dictapp.persistance;

import com.dictapp.persistance.base.BaseEntity;
import com.dictapp.persistance.user.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "dictionary")
@Getter
@Setter
public class DictionaryEntity extends BaseEntity {

    @Column(name = "create_date", updatable = false)
    @CreatedDate
    private Instant createDate;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "study_lang")
    private String studyLang;

    @Column(name = "translation_lang")
    private String translationLang;

    @Column(name = "client_uuid", nullable = false)
    private String clientUUID;
}
