package com.dictapp.persistance;

import com.dictapp.persistance.base.BaseEntity;
import com.dictapp.persistance.user.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "tags")
@Getter
@Setter
public class TagEntity extends BaseEntity {

    @Column(name = "create_date", updatable = false)
    @CreatedDate
    private Instant createDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "name")
    private String name;

    @Column(name = "color")
    private String color;

    @Column(name = "client_uuid", nullable = false)
    private String clientUUID;
}
