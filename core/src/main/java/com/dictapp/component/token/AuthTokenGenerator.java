package com.dictapp.component.token;

public interface AuthTokenGenerator {
    String USER_ID_KEY = "userId";

    String generateToken(String userId);
}
