package com.dictapp.component.token;

import com.dictapp.pojo.auth.SecretKeyHolder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AuthTokenGeneratorImpl implements AuthTokenGenerator {

    private final SecretKeyHolder secretKeyHolder;
    private final int tokenDuration;

    public AuthTokenGeneratorImpl(SecretKeyHolder secretKeyHolder, @Value("${app.token.duration}") int tokenDuration) {
        this.secretKeyHolder = secretKeyHolder;
        this.tokenDuration = tokenDuration;
    }

    @Override
    public String generateToken(String userId) {
        Date currentDate = new Date();
        return Jwts.builder()
                .setIssuedAt(currentDate)
                .setExpiration(getTokenExpirationDate(currentDate))
                .claim(USER_ID_KEY, userId)
                .signWith(SignatureAlgorithm.HS256, secretKeyHolder.getEncodedSecretKey())
                .compact();
    }

    private Date getTokenExpirationDate(Date currentDate) {
        return DateUtils.addMilliseconds(currentDate, tokenDuration);
    }
}
