package com.dictapp.converter;

import com.dictapp.dto.base.BaseDto;
import com.dictapp.persistance.base.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface PageMapper<T extends BaseEntity, R extends BaseDto> {

    /**
     * Converts persistence object to DTO object
     *
     * @param obj persistence object
     * @return dto
     */
    R convert(T obj);

    List<R> convertFromEntities(Collection<T> objects);

    T convert(R obj);

    List<T> convertToEntities(Collection<R> objects);

    default Page<R> convert(Page<T> page, Pageable pageable) {
        return new PageImpl<>(convertFromEntities(page.getContent()), pageable, page.getTotalElements());
    }
}
