package com.dictapp.converter;

import com.dictapp.converter.config.BaseMapperConfig;
import com.dictapp.converter.date.DateConverter;
import com.dictapp.dto.WordDto;
import com.dictapp.persistance.WordEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {DateConverter.class}, config = BaseMapperConfig.class)
public interface WordDtoConverter extends PageMapper<WordEntity, WordDto> {

    @Override
    @Mappings( {
            @Mapping(target = "user.id", source = "userId"),
            @Mapping(target = "dictionary.id", source = "dictionaryId"),
    })
    WordEntity convert(WordDto obj);

    @Override
    @InheritInverseConfiguration
    WordDto convert(WordEntity obj);
}
