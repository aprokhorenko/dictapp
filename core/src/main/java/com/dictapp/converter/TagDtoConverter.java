package com.dictapp.converter;

import com.dictapp.converter.config.BaseMapperConfig;
import com.dictapp.converter.date.DateConverter;
import com.dictapp.dto.TagDto;
import com.dictapp.persistance.TagEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {DateConverter.class}, config = BaseMapperConfig.class)
public interface TagDtoConverter extends PageMapper<TagEntity, TagDto> {

    @Override
    @Mappings( {
            @Mapping(target = "userId", source = "user.id")
    })
    TagDto convert(TagEntity tagEntity);

    @Override
    @InheritInverseConfiguration
    TagEntity convert(TagDto tagDto);
}
