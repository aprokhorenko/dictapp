package com.dictapp.converter;

import com.dictapp.converter.config.BaseMapperConfig;
import com.dictapp.converter.date.DateConverter;
import com.dictapp.dto.DictionaryDto;
import com.dictapp.persistance.DictionaryEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {DateConverter.class}, config = BaseMapperConfig.class)
public interface DictionaryDtoConverter extends PageMapper<DictionaryEntity, DictionaryDto> {

    @Override
    @Mappings( {
            @Mapping(target = "userId", source = "user.id")
    })
    DictionaryDto convert(DictionaryEntity dictionaryEntity);

    @Override
    @InheritInverseConfiguration
    DictionaryEntity convert(DictionaryDto obj);
}
