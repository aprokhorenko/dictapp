package com.dictapp.converter.date;

import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.util.Objects;

@UtilityClass
public class DateConverter {

    public static final long EMPTY_DATE = -1L;

    public static Instant convert(long timestamp) {
        if (timestamp > 0) {
            return Instant.ofEpochMilli(timestamp);
        } else {
            return null;
        }
    }

    public static long convert(Instant instant) {
        if (Objects.isNull(instant)) {
            return EMPTY_DATE;
        } else {
            return instant.toEpochMilli();
        }
    }
}
