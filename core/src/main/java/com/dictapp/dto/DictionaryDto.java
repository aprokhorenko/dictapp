package com.dictapp.dto;

import com.dictapp.dto.base.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class DictionaryDto extends BaseDto {

    private long createDate;
    @NotNull
    private String name;
    private String userId;
    @NotNull
    private String studyLang;
    @NotNull
    private String clientUUID;
    @NotNull
    private String translationLang;
}
