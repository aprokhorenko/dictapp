package com.dictapp.dto;

import com.dictapp.dto.base.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class WordDto extends BaseDto {

    private long createDate;
    private String userId;
    @NotNull
    private String word;
    @NotNull
    private String clientUUID;
    private boolean deleted;
    private boolean favorite;
    @NotNull
    private String dictionaryId;
    private String transcription;
    private String translation;
    @NotNull
    private String studyStatus;
    private int rating;
}
