package com.dictapp.dto.base;

import lombok.Data;

@Data
public abstract class BaseDto {

    private String id;
}
