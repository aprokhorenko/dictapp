package com.dictapp.rest.argument.resolver.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Objects;

public class JsonArgumentResolver implements HandlerMethodArgumentResolver {
    private static final String JSON_NODE_ATTRIBUTE = "JSON_NODE_ATTR";

    private final ObjectMapper objectMapper = new ObjectMapper()
            .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(JsonArg.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        JsonNode jsonNodeTree = getRequestBodyAsTree(webRequest);
        JsonArg jsonArg = parameter.getParameterAnnotation(JsonArg.class);
        JsonNode valueNode = jsonNodeTree.get(jsonArg.value());
        if (Objects.nonNull(valueNode)) {
            return deserializeJsonNode(valueNode, parameter.getGenericParameterType());
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private Object deserializeJsonNode(JsonNode valueNode, Type objectType) throws IOException {
        if (objectType instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) objectType;
            Class<?> rawType = (Class<?>) parameterizedType.getRawType();
            if (Collection.class.isAssignableFrom(rawType)) {
                return deserializeJsonNodeAsCollection(valueNode, parameterizedType);
            } else {
                return objectMapper.treeToValue(valueNode, rawType);
            }
        } else {
            return objectMapper.treeToValue(valueNode, (Class<?>) objectType);
        }
    }

    @SuppressWarnings("unchecked")
    private Object deserializeJsonNodeAsCollection(JsonNode valueNode, ParameterizedType paramType) throws IOException {
        Class<?> objectType = (Class<?>) paramType.getActualTypeArguments()[0];
        CollectionType type = objectMapper.getTypeFactory()
                .constructCollectionType(((Class<? extends Collection<?>>) paramType.getRawType()), objectType);
        return objectMapper.readValue(valueNode.toString(), type);
    }

    private JsonNode getRequestBodyAsTree(NativeWebRequest webRequest) {
        JsonNode jsonNodeTree = (JsonNode) webRequest.getAttribute(JSON_NODE_ATTRIBUTE, RequestAttributes.SCOPE_REQUEST);
        if (Objects.isNull(jsonNodeTree)) {
            try {
                HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
                String body = IOUtils.toString(servletRequest.getInputStream());
                jsonNodeTree = objectMapper.readTree(body);
                webRequest.setAttribute(JSON_NODE_ATTRIBUTE, jsonNodeTree, RequestAttributes.SCOPE_REQUEST);
                return jsonNodeTree;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return jsonNodeTree;
    }
}
