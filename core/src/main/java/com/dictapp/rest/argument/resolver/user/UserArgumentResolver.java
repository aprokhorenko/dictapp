package com.dictapp.rest.argument.resolver.user;

import com.dictapp.security.authentication.JwtAuthentication;
import com.dictapp.security.token.pojo.JwtTokenData;
import com.dictapp.service.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Objects;

@AllArgsConstructor
public class UserArgumentResolver implements HandlerMethodArgumentResolver {

    private final UserService userService;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(CurrentUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.nonNull(authentication)) {
            JwtAuthentication jwtAuthentication = (JwtAuthentication) authentication;
            JwtTokenData jwtTokenData = jwtAuthentication.getPrincipal();
            if (Objects.nonNull(jwtTokenData.getUserId())) {
                return userService.findById(jwtTokenData.getUserId());
            }
        }
        return null;
    }
}
