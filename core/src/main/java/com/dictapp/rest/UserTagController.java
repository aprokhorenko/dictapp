package com.dictapp.rest;

import com.dictapp.converter.TagDtoConverter;
import com.dictapp.dto.TagDto;
import com.dictapp.persistance.TagEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.rest.argument.resolver.user.CurrentUser;
import com.dictapp.service.tag.UserTagService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;

@RestController
@RequestMapping(API_V1_BASE_PATH + "tag")
@AllArgsConstructor
public class UserTagController {

    private final UserTagService userTagService;
    private final TagDtoConverter tagDtoConverter;

    @GetMapping(value = "all")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    public Page<TagDto> findAll(Pageable pageable,
                                @ApiIgnore @CurrentUser UserEntity user) {
        Page<TagEntity> tagEntities = userTagService.findAll(pageable, user);

        return tagDtoConverter.convert(tagEntities, pageable);
    }
}
