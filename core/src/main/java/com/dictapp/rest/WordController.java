package com.dictapp.rest;

import com.dictapp.converter.WordDtoConverter;
import com.dictapp.dto.WordDto;
import com.dictapp.persistance.WordEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.rest.argument.resolver.json.JsonArg;
import com.dictapp.rest.argument.resolver.user.CurrentUser;
import com.dictapp.service.word.WordService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;

@RestController
@RequestMapping(API_V1_BASE_PATH + "word")
@AllArgsConstructor
@Validated
public class WordController {

    private final WordService wordService;
    private final WordDtoConverter wordDtoConverter;

    @GetMapping(value = "all")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    public Page<WordDto> getAllWords(Pageable pageable,
                                     @ApiIgnore @CurrentUser UserEntity user) {
        Page<WordEntity> wordPage = wordService.findAll(pageable, user);
        return wordDtoConverter.convert(wordPage, pageable);
    }

    @PostMapping
    public List<WordDto> createOrUpdateWords(@JsonArg("words") @Valid List<WordDto> words,
                                             @ApiIgnore @CurrentUser UserEntity user) {
        List<WordEntity> wordsToSave = wordDtoConverter.convertToEntities(words);

        List<WordEntity> savedWords = wordService.createOrUpdateWords(wordsToSave, user);

        return wordDtoConverter.convertFromEntities(savedWords);
    }

    @DeleteMapping
    public ResponseEntity<Integer> deleteWords(@JsonArg("ids") List<String> wordIds,
                                               @ApiIgnore @CurrentUser UserEntity user) {
        int deleteCount = wordService.delete(wordIds, user);
        return ResponseEntity.ok(deleteCount);
    }
}
