package com.dictapp.rest;

import com.dictapp.converter.DictionaryDtoConverter;
import com.dictapp.dto.DictionaryDto;
import com.dictapp.persistance.DictionaryEntity;
import com.dictapp.persistance.user.UserEntity;
import com.dictapp.rest.argument.resolver.json.JsonArg;
import com.dictapp.rest.argument.resolver.user.CurrentUser;
import com.dictapp.service.dictionary.DictionaryService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;

@RestController
@RequestMapping(API_V1_BASE_PATH + "dictionary")
@AllArgsConstructor
@Validated
public class DictionaryController {

    private final DictionaryService dictionaryService;
    private final DictionaryDtoConverter dictionaryDtoConverter;

    @GetMapping(value = "all")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    public Page<DictionaryDto> getAllDictionaries(Pageable pageable,
                                                  @ApiIgnore @CurrentUser UserEntity user) {
        Page<DictionaryEntity> dictionaryEntities = dictionaryService.findAll(pageable, user);

        return dictionaryDtoConverter.convert(dictionaryEntities, pageable);
    }

    @PostMapping
    public List<DictionaryDto> createOrUpdateDictionaries(@JsonArg("dictionaries") @Valid List<DictionaryDto> dictionaries,
                                                          @ApiIgnore @CurrentUser UserEntity user) {
        List<DictionaryEntity> dictionaryEntities = dictionaryDtoConverter.convertToEntities(dictionaries);

        List<DictionaryEntity> savedDictionaries = dictionaryService.createOrUpdate(dictionaryEntities, user);

        return dictionaryDtoConverter.convertFromEntities(savedDictionaries);
    }

    @DeleteMapping
    public ResponseEntity<Integer> delete(@JsonArg("ids") List<String> dictionariesIds,
                                          @ApiIgnore @CurrentUser UserEntity user) {
        int deleteCount = dictionaryService.delete(dictionariesIds, user);
        return ResponseEntity.ok(deleteCount);
    }
}
