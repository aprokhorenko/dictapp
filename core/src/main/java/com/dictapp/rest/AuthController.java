package com.dictapp.rest;

import com.dictapp.persistance.auth.SessionEntity;
import com.dictapp.pojo.auth.AuthResponse;
import com.dictapp.service.auth.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;

@RestController
@RequestMapping(API_V1_BASE_PATH + "auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;

    @GetMapping(value = "facebook")
    public AuthResponse auth(@RequestParam(name = "token") String token) {
        SessionEntity sessionEntity = authService.authByFacebookToken(token);
        return new AuthResponse(sessionEntity.getToken());
    }
}
