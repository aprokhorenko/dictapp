package com.dictapp.rest.advisor;

import com.dictapp.exception.ExceptionResponseMessage;
import com.dictapp.exception.auth.AuthTokenException;
import com.dictapp.exception.facebook.FacebookAuthException;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Log4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleServerException(Exception e) {
        log.error(e.getMessage(), e);
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleRuntimeException(RuntimeException e) {
        if (log.isDebugEnabled()) {
            log.debug(e.getMessage(), e);
        }
        return new ResponseEntity<>(new ExceptionResponseMessage(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Set<String>> handleConstraintViolation(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();

        Set<String> messages = constraintViolations.stream()
                .map(constraintViolation -> String.format("%s value '%s' %s", constraintViolation.getPropertyPath(),
                        constraintViolation.getInvalidValue(), constraintViolation.getMessage()))
                .collect(Collectors.toSet());

        return new ResponseEntity<>(messages, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FacebookAuthException.class)
    public ResponseEntity<Object> handleAuthException(FacebookAuthException e) {
        return new ResponseEntity<>(e.getResponseMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AuthTokenException.class)
    public ResponseEntity<Object> handleUnauthorizedSessionException(AuthTokenException e) {
        return new ResponseEntity<>(e.getResponseMessage(), HttpStatus.UNAUTHORIZED);
    }
}
