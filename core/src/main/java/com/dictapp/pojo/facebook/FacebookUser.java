package com.dictapp.pojo.facebook;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FacebookUser {

    private final String id;
}
