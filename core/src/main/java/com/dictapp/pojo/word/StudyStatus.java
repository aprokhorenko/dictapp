package com.dictapp.pojo.word;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StudyStatus {
    NOT_INTERESTING(0),
    NEW(1),
    INTERESTING(2),
    ALREADY_KNOW(3);

    private final int value;
}
