package com.dictapp.pojo.auth;

import java.util.Base64;

public class SecretKeyHolder {

    private final String secretKey;
    private final byte[] encodedKey;

    public SecretKeyHolder(String secretKey) {
        this.secretKey = secretKey;
        this.encodedKey = Base64.getEncoder().encode(secretKey.getBytes());
    }

    public byte[] getEncodedSecretKey() {
        return encodedKey.clone();
    }

    public String getSecretKey() {
        return secretKey;
    }
}
