package com.dictapp.repository.word;

import com.dictapp.persistance.WordEntity;
import com.dictapp.persistance.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface WordRepository extends PagingAndSortingRepository<WordEntity, String> {

    Page<WordEntity> findAllByUserAndDeletedFalse(Pageable pageable, UserEntity user);

    List<WordEntity> findAllByClientUUIDInAndUser(Collection<String> clientUUID, UserEntity user);

    @Modifying
    @Query(value = "update WordEntity set deleted = true where id in (:ids) and user = :user")
    int deleteWordsByIds(@Param("ids") Collection<String> ids, @Param("user") UserEntity user);
}
