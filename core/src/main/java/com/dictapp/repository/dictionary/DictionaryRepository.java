package com.dictapp.repository.dictionary;

import com.dictapp.persistance.DictionaryEntity;
import com.dictapp.persistance.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface DictionaryRepository extends PagingAndSortingRepository<DictionaryEntity, String> {

    @Query(value = "select count(d) from DictionaryEntity d where d.id in (:ids) and d.user.id = :userId")
    int getCountDictionariesByUser(@Param("ids") Collection<String> ids,
                                   @Param("userId") String userId);

    Page<DictionaryEntity> findAllByUser(Pageable pageable, UserEntity user);

    List<DictionaryEntity> findAllByClientUUIDInAndUser(Collection<String> clientUUIDs, UserEntity user);

    @Modifying
    @Query(value = "delete from DictionaryEntity where id in (:ids) and user = :user")
    int deleteDictionariesByIds(@Param("ids") List<String> ids,
                                @Param("user") UserEntity user);
}
