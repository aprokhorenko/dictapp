package com.dictapp.repository.auth;

import com.dictapp.persistance.auth.SessionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface SessionRepository extends PagingAndSortingRepository<SessionEntity, String> {

    Optional<SessionEntity> findSessionEntityByToken(String token);
}
