package com.dictapp.repository.tag;

import com.dictapp.persistance.TagEntity;
import com.dictapp.persistance.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserTagRepository extends PagingAndSortingRepository<TagEntity, String> {

    Page<TagEntity> findAllByUser(Pageable pageable, UserEntity user);
}
