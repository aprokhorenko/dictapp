package com.dictapp.repository.user;

import com.dictapp.persistance.user.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, String> {

    Optional<UserEntity> findUserEntityByFacebookId(String facebookId);
}
