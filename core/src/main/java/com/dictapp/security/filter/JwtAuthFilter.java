package com.dictapp.security.filter;

import com.dictapp.security.authentication.JwtAuthentication;
import com.dictapp.security.token.pojo.JwtToken;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Pattern;

@AllArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {

    public static final String TOKEN = "token";
    private static final Pattern AUTH_BEARER_PATTERN = Pattern.compile("^Bearer$", Pattern.CASE_INSENSITIVE);

    private final AuthenticationEntryPoint entryPoint;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String jwtToken = getTokenFromRequest(request);
        if (Objects.nonNull(jwtToken) && Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
            try {
                Authentication authentication = new JwtAuthentication(new JwtToken(jwtToken));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (AuthenticationException failed) {
                SecurityContextHolder.clearContext();
                entryPoint.commence(request, response, failed);
                return;
            } catch (Exception e) {
                SecurityContextHolder.clearContext();
                logger.error(e.getMessage(), e);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    private String getTokenFromRequest(HttpServletRequest httpRequest) {
        final String authorizationHeader = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (Objects.isNull(authorizationHeader)) {
            return null;
        }
        final String[] parts = authorizationHeader.split("\\s");
        if (parts.length != 2) {
            // "Format - Authorization: Bearer [token]"
            return null;
        }
        final String scheme = parts[0];
        final String credentials = parts[1];
        return AUTH_BEARER_PATTERN.matcher(scheme).matches() ? credentials : null;
    }
}
