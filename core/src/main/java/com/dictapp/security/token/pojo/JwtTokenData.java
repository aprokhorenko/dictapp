package com.dictapp.security.token.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class JwtTokenData {

    private final String userId;
}
