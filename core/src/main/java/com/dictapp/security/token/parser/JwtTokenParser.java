package com.dictapp.security.token.parser;

import com.dictapp.security.token.pojo.JwtTokenData;

public interface JwtTokenParser {

    JwtTokenData parse(String token);
}
