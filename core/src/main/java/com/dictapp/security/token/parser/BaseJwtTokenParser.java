package com.dictapp.security.token.parser;

import com.dictapp.pojo.auth.SecretKeyHolder;
import com.dictapp.security.token.pojo.JwtTokenData;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.AllArgsConstructor;

import static com.dictapp.component.token.AuthTokenGenerator.USER_ID_KEY;

@AllArgsConstructor
public class BaseJwtTokenParser implements JwtTokenParser {

    private final SecretKeyHolder secretKeyHolder;

    @Override
    public JwtTokenData parse(String token) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
        Claims claims = Jwts.parser()
                .setSigningKey(secretKeyHolder.getEncodedSecretKey())
                .parseClaimsJws(token)
                .getBody();

        String userId = claims.get(USER_ID_KEY, String.class);

        return new JwtTokenData(userId);
    }
}
