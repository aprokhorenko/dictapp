package com.dictapp.security.provider;

import com.dictapp.exception.ExceptionMessages;
import com.dictapp.exception.auth.AuthTokenException;
import com.dictapp.persistance.auth.SessionEntity;
import com.dictapp.security.authentication.JwtAuthentication;
import com.dictapp.security.token.parser.JwtTokenParser;
import com.dictapp.security.token.pojo.JwtToken;
import com.dictapp.security.token.pojo.JwtTokenData;
import com.dictapp.service.auth.AuthService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.Optional;

@AllArgsConstructor
@Log4j
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private static final AuthTokenException WRONG_TOKEN_EXCEPTION = new AuthTokenException(ExceptionMessages.WRONG_TOKEN_MESSAGE);
    private final AuthService authService;
    private final JwtTokenParser tokenParser;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtAuthentication jwtAuthentication = (JwtAuthentication) authentication;
        JwtToken jwtToken = jwtAuthentication.getCredentials();

        JwtTokenData tokenData;
        try {
            final String token = jwtToken.getToken();
            Optional<SessionEntity> sessionOpt = authService.getSessionByToken(token);
            if (!sessionOpt.isPresent()) {
                throw WRONG_TOKEN_EXCEPTION;
            }
            tokenData = tokenParser.parse(token);
        } catch (ExpiredJwtException e) {
            throw new AuthTokenException(ExceptionMessages.EXPIRE_TOKEN_MESSAGE);
        } catch (MalformedJwtException | SignatureException | UnsupportedJwtException | IllegalArgumentException e) {
            throw WRONG_TOKEN_EXCEPTION;
        }
        jwtAuthentication.setPrincipal(tokenData);
        jwtAuthentication.setAuthenticated(true);
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthentication.class.equals(authentication);
    }
}
