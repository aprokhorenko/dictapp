package com.dictapp.security.authentication;

import com.dictapp.security.token.pojo.JwtToken;
import com.dictapp.security.token.pojo.JwtTokenData;
import org.springframework.security.authentication.AbstractAuthenticationToken;

public class JwtAuthentication extends AbstractAuthenticationToken {

    private final JwtToken jwtToken;
    private JwtTokenData jwtTokenData;

    public JwtAuthentication(JwtToken jwtToken) {
        super(null);
        this.jwtToken = jwtToken;
        setAuthenticated(false);
    }

    @Override
    public JwtToken getCredentials() {
        return jwtToken;
    }

    @Override
    public JwtTokenData getPrincipal() {
        return jwtTokenData;
    }

    public void setPrincipal(JwtTokenData jwtTokenData) {
        this.jwtTokenData = jwtTokenData;
    }
}
