package com.dictapp.exception;

public interface ExceptionMessages {

    String FACEBOOK_WRONG_AUTH_MESSAGE = "Wrong credential!";
    String EXPIRE_TOKEN_MESSAGE = "Token is expired!";
    String MISSING_TOKEN_MESSAGE = "Missing token!";
    String WRONG_TOKEN_MESSAGE = "Wrong token!";
}
