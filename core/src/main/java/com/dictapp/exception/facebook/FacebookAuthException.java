package com.dictapp.exception.facebook;

import com.dictapp.exception.ExceptionResponseMessage;
import lombok.Getter;

@Getter
public class FacebookAuthException extends RuntimeException {

    public final ExceptionResponseMessage responseMessage;

    public FacebookAuthException(String message) {
        this.responseMessage = new ExceptionResponseMessage(message);
    }
}
