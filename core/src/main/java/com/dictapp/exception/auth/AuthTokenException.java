package com.dictapp.exception.auth;

import com.dictapp.exception.ExceptionResponseMessage;
import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class AuthTokenException extends AuthenticationException {

    public final ExceptionResponseMessage responseMessage;

    public AuthTokenException(String msg) {
        super(msg);
        responseMessage = new ExceptionResponseMessage(msg);
    }
}
