package com.dictapp.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor
@Getter
public class ExceptionResponseMessage {
    public static final ExceptionResponseMessage EMPTY = new ExceptionResponseMessage(StringUtils.EMPTY);

    private final String message;
}
