package com.dictapp.configuration;

import com.dictapp.ApplicationConstants;
import com.dictapp.rest.argument.resolver.json.JsonArgumentResolver;
import com.dictapp.rest.argument.resolver.user.UserArgumentResolver;
import com.dictapp.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {

    private final UserService userService;
    private final boolean hasDevProfile;

    public WebConfiguration(Environment environment, UserService userService) {
        this.userService = userService;
        this.hasDevProfile = Stream.of(environment.getActiveProfiles())
                .anyMatch(profile -> Objects.equals(profile, ApplicationConstants.PROFILE_DEV));
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON_UTF8);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new JsonArgumentResolver());
        argumentResolvers.add(new UserArgumentResolver(userService));
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        if (hasDevProfile) {
            registry.addRedirectViewController("/documentation/v2/api-docs", "/v2/api-docs").setKeepQueryParams(true);
            registry.addRedirectViewController("/documentation/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui");
            registry.addRedirectViewController("/documentation/swagger-resources/configuration/security", "/swagger-resources/configuration/security");
            registry.addRedirectViewController("/documentation/swagger-resources", "/swagger-resources");
        }
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (hasDevProfile) {
            registry.addResourceHandler("/documentation/**").addResourceLocations("classpath:/META-INF/resources/");
        }
    }

}
