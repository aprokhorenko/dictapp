package com.dictapp.configuration;

import com.dictapp.configuration.security.ApplicationSecurityConfigurer;
import com.dictapp.pojo.auth.SecretKeyHolder;
import com.dictapp.security.authentication.JwtAuthenticationEntryPoint;
import com.dictapp.security.filter.CORSFilter;
import com.dictapp.security.filter.JwtAuthFilter;
import com.dictapp.security.provider.JwtAuthenticationProvider;
import com.dictapp.security.token.parser.BaseJwtTokenParser;
import com.dictapp.security.token.parser.JwtTokenParser;
import com.dictapp.service.auth.AuthService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration(exclude = SecurityAutoConfiguration.class)
public class SecurityConfiguration {

    @Bean
    @Primary
    public WebSecurityConfigurerAdapter applicationSecurityConfigurer(@Qualifier("jwtAuthProvider") AuthenticationProvider authenticationProvider) {
        JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint = new JwtAuthenticationEntryPoint();
        return new ApplicationSecurityConfigurer(new JwtAuthFilter(jwtAuthenticationEntryPoint), new CORSFilter(), jwtAuthenticationEntryPoint, authenticationProvider);
    }

    @Bean(name = "jwtAuthProvider")
    public AuthenticationProvider jwtAuthProvider(AuthService authService, JwtTokenParser jwtTokenParser) {
        return new JwtAuthenticationProvider(authService, jwtTokenParser);
    }

    @Bean
    public JwtTokenParser jwtTokenParser(SecretKeyHolder secretKeyHolder) {
        return new BaseJwtTokenParser(secretKeyHolder);
    }

    @Bean
    @ConditionalOnMissingBean
    public SecretKeyHolder secretKeyHolder(@Value("${app.auth.secret.key}") String secretKey) {
        return new SecretKeyHolder(secretKey);
    }
}
