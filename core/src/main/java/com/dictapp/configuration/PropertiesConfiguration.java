package com.dictapp.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:default.properties")
@PropertySource(value = "classpath:properties/${spring.profiles.active}/database.properties")
@PropertySource(value = "classpath:properties/${spring.profiles.active}/override.properties", ignoreResourceNotFound = true)
@PropertySource(value = "file:${app.properties.override.path}/override-${spring.profiles.active}.properties", ignoreResourceNotFound = true)
public class PropertiesConfiguration {
}
