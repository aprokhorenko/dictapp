package com.dictapp.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
@EnableAspectJAutoProxy
@EntityScan(basePackages = "com.dictapp.persistance", basePackageClasses = Jsr310JpaConverters.class)
public class CoreConfiguration {
}
