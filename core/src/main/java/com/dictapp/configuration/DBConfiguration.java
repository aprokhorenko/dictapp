package com.dictapp.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class DBConfiguration {

    @Bean(destroyMethod = "close")
    @ConditionalOnMissingBean
    @ConfigurationProperties("db")
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .build();
    }
}
