package com.dictapp.configuration.security;

import com.dictapp.ApplicationConstants;
import com.dictapp.security.authentication.JwtAuthenticationEntryPoint;
import com.dictapp.security.filter.CORSFilter;
import com.dictapp.security.filter.JwtAuthFilter;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;

import java.util.Objects;
import java.util.stream.Stream;

import static com.dictapp.ApplicationConstants.API_V1_BASE_PATH;

public class ApplicationSecurityConfigurer extends WebSecurityConfigurerAdapter {

    private final JwtAuthFilter jwtAuthFilter;
    private final CORSFilter corsFilter;
    private final JwtAuthenticationEntryPoint authenticationEntryPoint;
    private final AuthenticationProvider authenticationProvider;

    public ApplicationSecurityConfigurer(JwtAuthFilter jwtAuthFilter,
                                         CORSFilter corsFilter,
                                         JwtAuthenticationEntryPoint authenticationEntryPoint,
                                         AuthenticationProvider authenticationProvider) {
        this.jwtAuthFilter = jwtAuthFilter;
        this.corsFilter = corsFilter;
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.authenticationProvider = authenticationProvider;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        ApplicationContext applicationContext = getApplicationContext();
        Environment environment = applicationContext.getEnvironment();
        boolean hasDevProfile = Stream.of(environment.getActiveProfiles())
                .anyMatch(profile -> Objects.equals(profile, ApplicationConstants.PROFILE_DEV));
        if (hasDevProfile) {
            web.ignoring()
                    .antMatchers("/documentation/**",
                            "/v2/api-docs",
                            "/configuration/ui",
                            "/swagger-resources/**",
                            "/configuration/**",
                            "/swagger-ui.html",
                            "/webjars/**",
                            "/swagger/**");
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // we don't need CSRF because our token is invulnerable
                .csrf().disable()
                .authenticationProvider(authenticationProvider)
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and()
                // don't need to create session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers(API_V1_BASE_PATH + "auth/**").permitAll()
                .anyRequest().authenticated().and()
                // Custom JWT based security filter
                .addFilterAfter(jwtAuthFilter, SecurityContextPersistenceFilter.class)
                .addFilterBefore(corsFilter, JwtAuthFilter.class)
                // disable page caching
                .headers().cacheControl();
    }
}
